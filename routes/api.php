<?php


use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



// Route::get("/user","UserController@index");
// Route::get("/user","TemaController@index");
//  Route::post("/user","TemaController@store");
//  Route::get("/user/{user}","TemaController@show");
//  Route::put("/user/{user}","TemaController@update");
//  Route::delete("/user/{user}","TemaController@destroy");



// Route::get("/temas","TemaController@index");
// Route::post("/temas","TemaController@store");
// Route::post("/temas_eliminar","TemaController@destroy");
// Route::get("/temas/{id}","TemaController@show");
// Route::post("/temas","TemaController@store");
// Route::put("/temas/{id}","TemaController@update");
// Route::post("/temas/eliminar","TemaController@destroy");


#temas
Route::get("/temas","TemaController@index");
Route::post("/temas","TemaController@store");
Route::post("/temas_eliminar","TemaController@destroy");
#comentarios
Route::get("/comentarios/{tema}","ComentarioController@index");
Route::post("/comentarios/create","ComentarioController@store");
Route::post("/comentarios_eliminar","ComentarioController@destroy");

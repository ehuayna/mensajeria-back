<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    protected $table        = "personas";
    protected $fillable = [
        'nombres', 'apellido_paterno', 'apellido_materno',
    ];

    public $timestamps = false;
}

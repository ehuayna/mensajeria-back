<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
  protected $table        = "temas";
  protected $fillable = [
      'titulo', 'descripcion', 'estado','user_id'
  ];

  public function user(){
    return $this->belogsTo(User::class);
  }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tema;
use DB;

class TemaController extends Controller
{


    public function index(){
      try{
              $temas = DB::table('temas as t')
              ->leftjoin('comentarios as c', 't.id', '=', 'c.tema_id')
              ->leftjoin('users as u', 't.user_id', '=', 'u.user_id')
              ->leftjoin('personas as p', 'u.persona_id', '=', 'p.id')
              ->select('t.titulo as tema_titulo',
                       't.id as tema_id',
                       'p.nombres as persona_nombres',
                       't.created_at',
                        DB::raw('count(c.id) as comentarios_total')
                     )
              ->groupBy('t.id', 't.titulo','p.nombres','created_at')
              ->get();


                // select t.id,t.titulo,count(c.id),p.nombres,t.created_at from temas as t
                // left join comentarios as c
                // on t.id = c.tema_id
                // left join users as u
                // on t.user_id = u.id
                // left join personas as p
                // on u.persona_id=p.id
                // GROUP BY t.id, t.titulo,p.nombres,t.created_at

        return response()->json(['msg' => 'Operaciòn realizada con èxito', 'success' => true,'rpta'=>$temas], 201);

      }catch(\Exception $e){
          return response()->json(['msg' => 'Error al mostrar', 'success' => false, 'error'=>$e], 201);
      }

    }

    public function store(Request $request){

        try{

          $json = $request->input("json");
          $data = json_decode($json,true);

            $tema = new Tema();
            $tema->titulo = $data["titulo"];
            $tema->descripcion = $data["descripcion"];
            $tema->estado = 1;
            $tema->user_id = 2;
            $tema->save();

            return response()->json(['msg' => 'Operaciòn realizada con èxito', 'success' => true], 201);

        }catch(\Exception $e){
            return response()->json(['msg' => 'Error al registrar', 'success' => false, 'error'=>$e], 201);
        }

    }


    public function destroy(Request $request){
      try{

        $json = $request->input("json");
        $data = json_decode($json,true);

        $tema = Tema::findOrFail($data['id']);
        $tema->delete();

        return response()->json(['msg' => 'Se ha eliminado el registro','success' => true ], 201);

      }catch(\Exception $e){
        return response()->json(['msg' => 'No se pudo eliminar el registro', 'success' => false, 'error'=>$e], 201);
      }

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Comentario;
use DB;

class ComentarioController extends Controller
{
    public function index($tema){
      try{
        // $comentarios = Comentario::select('id as comentario_id','descripcion','user_id')
        //                           ->where('tema_id',$tema)
        //                           ->orderBy('comentario_id','DESC')
        //                           ->get();

                                  $comentarios = DB::table('comentarios as c')
                                  ->join('users as u', 'c.user_id', '=', 'u.user_id')
                                  ->join('temas as t', 'c.tema_id', '=', 't.id')
                                  ->join('personas as p', 'u.persona_id', '=', 'p.id')
                                  ->select('c.id as comentario_id',
                                           'c.descripcion as descripcion',
                                           'p.nombres as persona_nombres',
                                           'c.user_id as user_id',
                                           'c.created_at as created_at'
                                           )
                                  ->where('tema_id',$tema)
                                  ->get();
                                  //
                                  // dd($comentarios);

        return response()->json(['msg' => 'Operaciòn realizada con èxito', 'success' => true,'rpta' => $comentarios], 201);

      }catch(\Exception $e){
        return response()->json(['msg' => 'Error al mostrar', 'success' => false, 'error'=>$e], 201);
      }
    }

    public function store(Request $request){
        try{

          $json = $request->input("json");
          $data = json_decode($json,true);

            $comentario = new Comentario();
            $comentario->descripcion = $data["descripcion"];
            $comentario->tema_id =  $data["tema_id"];
            $comentario->user_id = 5;
            $comentario->estado = 1;
            $comentario->save();
            // dd($comentario);

            return response()->json(['msg' => 'Operaciòn realizada con èxito', 'success' => true,'rpta' => $comentario], 201);

        }catch(\Exception $e){
            return response()->json(['msg' => 'Error al registrar', 'success' => false, 'error'=>$e], 201);
        }

    }

    public function destroy(Request $request){
      try{
        $json = $request->input("json");
        $data = json_decode($json,true);

        $tema = Comentario::findOrFail($data['id']);
        $tema->delete();

        return response()->json(['msg' => 'Se ha eliminado el registro','success' => true ], 201);

      }catch(\Exception $e){
        return response()->json(['msg' => 'No se pudo eliminar el registro', 'success' => false, 'error'=>$e], 201);
      }

    }
}

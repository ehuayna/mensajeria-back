<?php namespace App\Http\Middleware;

use Closure;

class CORS {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        header("Access-Control-Allow-Origin: *");
        //
        // if (isset($_SERVER['HTTP_ORIGIN'])) {
        // // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        // header('Access-Control-Allow-Credentials: true');
        // header('Access-Control-Max-Age: 86400');
        // }

        // ALLOW OPTIONS METHOD
        $headers = [
            // 'Access-Control-Allow-Origin'=> '*',
              // if (isset($_SERVER['HTTP_ORIGIN'])) {
        // // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        // header('Access-Control-Allow-Credentials: true');
        // header('Access-Control-Max-Age: 86400');
        // }
            'Access-Control-Allow-Methods'=> 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers'=> 'Content-Type, X-Auth-Token, Origin',
            'Content-type: application/json'

   // 
   //          header('Cache-Control: no-cache, must-revalidate');
   // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   // header('Content-type: application/json');
   // header('Access-Control-Allow-Headers: Content-Type');
   // header("Access-Control-Allow-Origin: *");
            // 'Allow', 'GET, POST, PUT, DELETE, OPTIONS, HEAD'
        ];
        if($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            return Response::make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);
        return $response;
    }

}

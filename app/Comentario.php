<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
  protected $table        = "comentarios";

  protected $fillable = [
      'tema_id',
      'user_id',
      'estado',
      'descripcion',
  ];

  // public $timestamps = false;

  public function user(){
    return $this->belogsTo(User::class);
  }

  public function tema(){
    return $this->belogsTo(Tema::class);
  }
}

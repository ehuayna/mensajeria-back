<?php

use Illuminate\Database\Seeder;
//
use App\Tema;
use App\Persona;
use App\User;
use App\Comentario;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        factory(Persona::class,5)->create();
        factory(User::class,5)->create();
        factory(Tema::class,5)->create();
        factory(Comentario::class,20)->create();

    }
}

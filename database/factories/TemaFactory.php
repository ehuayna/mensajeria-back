<?php

use Faker\Generator as Faker;


use App\User;

$factory->define(App\Tema::class, function (Faker $faker) {
    return [
      'titulo' => $faker->sentence,
      'descripcion' => $faker->text($maxNbChars = 200),
      'estado' => $faker->randomElement(['1','0']),
      // 'user_id' => factory('App\User')->create()->id,
      'user_id' => function(){
        return User::all()->random();
      }
    ];
});

<?php

use Faker\Generator as Faker;

use App\User;
use App\Tema;

$factory->define(App\Comentario::class, function (Faker $faker) {
    return [

      'descripcion' => $faker->text($maxNbChars = 200),
      'estado' => $faker->randomElement(['1','0']),
      'user_id' => function(){
        return User::all()->random();
      },
      'tema_id' => function(){
        return Tema::all()->random();
      }
    ];
});

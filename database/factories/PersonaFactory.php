<?php

use Faker\Generator as Faker;



$factory->define(App\Persona::class, function (Faker $faker) {
    return [
        // 'name' => $faker->name,
        'nombres' => $faker->name,
        // 'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'apellido_paterno' => $faker->firstName,
        'apellido_materno' => $faker->lastName,
    ];
});
